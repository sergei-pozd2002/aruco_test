#include "ros/ros.h"
#include "opencv4/opencv2/aruco.hpp"
#include "opencv4/opencv2/highgui.hpp"

cv::Ptr<cv::aruco::Dictionary> markerDictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_4X4_50);

//std::string MarkerImageCreate(int markerNum) {
//    cv::Mat markerImage;
//    cv::aruco::drawMarker(markerDictionary, markerNum, 200, markerImage, 1);
//    std::string imagePath = "/home/sergei-pozd/ROS1/tests/src/aruco/" + std::to_string(markerNum) + ".jpg";
//    cv::imwrite(imagePath, markerImage);
//    //ROS_INFO_STREAM("marker " << markerNum << " saved");
//    return imagePath;
//}

void MarkerDetector(cv::Mat inputImage/*, int markerNum*/) {
    std::vector<int> markerIDs;
    std::vector<std::vector<cv::Point2f>> markerCorners, rejectedMarkers;
    cv::Ptr<cv::aruco::DetectorParameters> params = cv::aruco::DetectorParameters::create();
    cv::aruco::detectMarkers(inputImage, markerDictionary, markerCorners, markerIDs, params, rejectedMarkers);

    cv::Mat outputImage = inputImage.clone();
    cv::aruco::drawDetectedMarkers(outputImage, markerCorners, markerIDs);
    std::string outputPath = "/home/sergei-pozd/ROS1/tests/src/aruco/"/* + std::to_string(markerNum) + */"detected.jpg";
    cv::imwrite(outputPath, outputImage);
    cv::namedWindow("out", 0);
    cv::imshow("", outputImage);
}

int main() {
    cv::Mat inputImage;
;
    std::string imagePath = "/home/sergei-pozd/ROS1/tests/src/aruco/images/aruco_3.jpg";
    inputImage = cv::imread(imagePath, cv::IMREAD_COLOR);
    MarkerDetector(inputImage);
    cv::waitKey(0);
    cv::destroyAllWindows();
    return 0;
}
