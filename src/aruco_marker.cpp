#include "opencv2/core.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/aruco.hpp"
#include "opencv2/calib3d.hpp"
#include "iostream"
#include "fstream"
#include "ros/ros.h"

void createMarkers() {
    cv::Mat outputMarker;
    outputMarker = cv::imread("/home/sergei-pozd/ROS1/tests/src/aruco/images/aruco_2.jpg");
    //cv::resize(outputMarker, outputMarker, cv::Size(), 0.3, 0.3, cv::INTER_LINEAR);
    std::cout << outputMarker.cols << ' ' << outputMarker.rows << std::endl;
    cv::Ptr<cv::aruco::Dictionary> markerDict = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_4X4_50);
//    for (int i = 0; i < 50; i++) {
//        cv::aruco::drawMarker(markerDict, i, 500, outputMarker, 1);
//        std::ostringstream convert;
//        std::string imageName = "/home/sergei-pozd/ROS1/tests/src/aruco/markers_4*4/4*4_";
//        convert << imageName << i << ".jpg";
//        cv::imwrite(convert.str(), outputMarker);
//    }
    std::vector<std::vector<cv::Point2f>> corners;
    std::vector<int> ids;
    cv::aruco::detectMarkers(outputMarker, markerDict, corners, ids);
    if (ids.size() > 0)
        cv::aruco::drawDetectedMarkers(outputMarker, corners, ids);
    //std::cout << corners.size() << std::endl;

    std::ofstream out;
    out.open("/home/sergei-pozd/ROS1/tests/src/aruco/config/aruco.yaml", std::ios::app);
    if (out.is_open()) {
        out << "size_2: " << corners.size() * 2 <<std::endl;
    }
    out.close();  // затестить чтобы записывать параметры и потом считывать

    int int_var, int_size;
    std::string str_var, str_param;
    if (ros::param::has("/aruco/param_1")) {
        ros::param::get("/aruco/param_1", int_var);
        std::cout << int_var << std::endl;
    }
    if (ros::param::has("/aruco/param_2")) {
        ros::param::get("/aruco/param_2", str_var);
        std::cout << str_var << std::endl;
    }
    if (ros::param::has("/aruco/size_2")) {
        ros::param::get("/aruco/size_2", int_size);
        std::cout << int_size << std::endl;
    }

    for (size_t i = 0; i < corners.size(); i++) {
        std::cout << "ids=" << ids[i] << ": ";
        for (size_t j = 0; j < 4; j++) {
            std::cout << "|j=" << j << ": " << corners[i][j].x << ' ' << corners[i][j].y;
            cv::circle(outputMarker, cv::Point(corners[i][j].x, corners[i][j].y), 5, cv::Scalar(0, 255, 0), 1);
            cv::putText(outputMarker, std::to_string(ids[i]), cv::Point(corners[i][j].x, corners[i][j].y), 1, 1, cv::Scalar(0, 0, 255), 1, 1);
            }
        std::cout << std::endl;
    }
//    cv::namedWindow("out", 0);
//    cv::imshow("out", outputMarker);
    cv::imwrite("/home/sergei-pozd/ROS1/tests/src/aruco/detected/image_detected_" + std::to_string(ros::Time::now().toSec()) + ".png", outputMarker);
}

int main(int argc, char** argv) {
    ros::init(argc, argv, "aruco_marker");
    ros::NodeHandle nh;
    //ros::Rate rate(0.1);
    //rate.sleep();
    //ros::Duration(10).sleep();
    createMarkers();
    cv::waitKey(0);
    cv::destroyAllWindows();
}
